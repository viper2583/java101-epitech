package net.epitech.java.td01.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

//je suis une class de type controller dans spring = je peux être injectée et je fais office de servlet.
@Controller
public class HomeController {

	// je récupère tout ce qui vient sur l'url / à la racine de l'application
	@RequestMapping(value = "/")
	public ModelAndView test(HttpServletResponse response) throws IOException {
		// j'indique que la vue recherchée est home, et délègue à un
		// ViewResolver le soins de trouver la JSP correspondante
		return new ModelAndView("home");
	}
}
